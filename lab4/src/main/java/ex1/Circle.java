package ex1;

public class Circle {
    private double radius;
    private String color;
    public Circle(){
        this.radius=1.0;
        this.color="red";
    }
    public Circle(double r){
        radius = r;
        color ="red";
    }
    public Circle(double r,String color1){
        radius =r;
        this.color=color1;
    }
    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return radius*radius*Math.PI;
    }
    public String getColor(){
        return color;
    }

}
