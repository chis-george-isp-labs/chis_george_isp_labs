package ex5;

import ex1.Circle;

public class Cylinder extends Circle{
    private double height=1.0;
    public Cylinder(double radius){
    super(radius,null);
    }
    public Cylinder(double radius,double height){
        super(radius,"red");
        this.height=height;
    }

    public double getHeight() {
        return height;
    }
    public double getVolume(){
        return super.getArea()*height;
    }
    public double getArea(){
        return 2*super.getArea()*2*Math.PI+super.getRadius()*height;
    }
}
