package ex6;
public class Circle1 extends Shape{
    private static double radius;
    private String color;
    private boolean filled;

    public Circle1(){
        this.radius=1.0;
    }
    public Circle1(double radius){
        this.radius=radius;

    }
    public Circle1(double radius,String color1,Boolean filled){
        this.radius =radius;
        this.color=color1;
        if(filled==false)this.filled=false;
        else this.filled=true;
    }
    public static double getRadius(){
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return radius*radius*Math.PI;
    }
    public double getPerimeter(){
        return 2*Math.PI*getRadius();
    }
    public String toString(){
        return "A Circle with radius ="+getRadius()+"which is a subclass of "+super.toString();
    }
}

