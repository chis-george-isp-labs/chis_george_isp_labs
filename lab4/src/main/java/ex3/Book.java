package ex3;

import ex2.Author;

public class Book {
    private static String name;
    private String author;
    private double price;
    private int qtylnStock;

    public Book(String name, Author author,double price) {
    this.name= name;
    this.price=price;
        this.author=author.getName();
    }
    public Book(String name, Author author,double price,int qtylnStock) {
        this.name= name;
        this.price=price;
        this.qtylnStock=qtylnStock;
        this.author=author.getName();
    }

    public double getPrice() {
        return price;
    }

    public int getQtylnStock() {
        return qtylnStock;
    }

    public String getAuthor() {
        return author;
    }

    public static String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtylnStock(int qtylnStock) {
        this.qtylnStock = qtylnStock;
    }
    public String toString(){

        return " '"+Book.getName()+"'  by "+Author.getName()+"("+Author.getGender()+")"+" at "+Author.getEmail();
    }
}
