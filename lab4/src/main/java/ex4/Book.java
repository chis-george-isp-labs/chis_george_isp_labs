package ex4;

import ex2.Author;

public class Book {
    private static String name;
    private double authors;
    private double price;
    private int qtylnStock;

    public Book(String name, Author[] authors,double price) {
        this.name= name;
        this.price=price;
        this.authors=authors.getNumbers();
    }
    public Book(String name, Author[] authors,double price,int qtylnStock) {
        this.name= name;
        this.price=price;
        this.qtylnStock=qtylnStock;
        this.authors=authors.getNumbers();
    }

    public double getPrice() {
        return price;
    }

    public int getQtylnStock() {
        return qtylnStock;
    }

    public double getAuthor() {
        return authors;
    }

    public static String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtylnStock(int qtylnStock) {
        this.qtylnStock = qtylnStock;
    }
    public String toString(){

        return " '"+ Book.getName()+"'  by "+Author.getNumbers()+" Authors";
    }
}
