package ex2;

public class Author {
    private static String name;
    private static String email;
    private static char gender;
    private static double numbers;
    public Author(String name1,String email1,char gender1,double numbers){
        this.name=name1;
        this.email=email1;
        if(gender1=='f' || gender1=='m')
            this.gender=gender1;
        else System.out.println("Choose the gender, type f or m.");
        this.numbers=numbers;
    }

    public static double getNumbers() {
        return numbers;
    }

    public void setEmail(String email1){
        this.email = email1;
    }
    public static String getName(){
        return name;
    }
    public static String getEmail(){

        return email;
    }
    public static char getGender(){

        return gender;
    }
    public String toString(){
        return this.name + " "+ this.gender +" at " +this.email;
    }
}
