package ex1;


public class Circle extends Shape{
    private static double radius;
    private String color;
    private boolean filled;

    public Circle(){
        this.radius=1.0;
    }
    public Circle(double radius){
        this.radius=radius;

    }
    public Circle(double radius,String color1,Boolean filled){
        this.radius =radius;
        this.color=color1;
        if(filled==false)this.filled=false;
        else this.filled=true;
    }
    public static double getRadius(){
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    double getArea(){

        return radius*radius*Math.PI;
    }
    double getPerimeter(){

        return 2*Math.PI*getRadius();
    }
    public String toString(){
        return "A Circle with radius ="+getRadius()+"which is a subclass of "+super.toString();
    }
}

