package ex1;

public class Square extends Rectangle{
    public Square(){

    }
    public Square(double side){
        super(side,side);
    }
    public Square(double side,String color,boolean filled){
        super(side,side,color,filled);
    }
    public double getSide(){
        return this.getLength();
    }
    public void setSide(double side){
        super.setWidth(side);
        super.setLength(side);
    }
    public void setWidth(double side){
        super.setWidth(side);
    }
    public void setLength(double side){
        super.setLength(side);
    }
    public String toString(){
        super.toString();
        return"A square with side of "+getLength()+" which is a subclass of "+super.toString();
    }
}
