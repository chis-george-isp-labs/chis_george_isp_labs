package ex4;

import java.io.*;

public class Main {
    private String fileToStoreObjects="src/main/resources/cars.txt";

    public static void main(String[] args) {
        Main main=new Main();
        Car car=new Car("S-Klass",100_000D);
        main.serializeCar(car);
        System.out.println("Object serialized "+car);
        System.out.println("Deserialized object "+main.deserializeCar());
    }
    private void serializeCar(Car car){
        try(ObjectOutputStream o =new ObjectOutputStream(new FileOutputStream(fileToStoreObjects));){
            o.writeObject(car);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    private Car deserializeCar(){
        try(ObjectInputStream in =new ObjectInputStream(new FileInputStream(fileToStoreObjects))){
            return (Car) in.readObject();
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }

}
