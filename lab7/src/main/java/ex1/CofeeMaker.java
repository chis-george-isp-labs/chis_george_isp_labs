package ex1;



public class CofeeMaker {
    private final int nrOfCoffees;

    public CofeeMaker(int nrOfCoffees) {
        this.nrOfCoffees = nrOfCoffees;
    }

    Coffee makeCofee() throws  Exception{
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee;
       try {
            coffee = new Coffee(t, c);
       } catch (Exception e) {
           System.out.println("Too many coffees");
       }
       finally {
           System.out.println("Enjoy your daily coffee");
       }
        return null;
    }

}