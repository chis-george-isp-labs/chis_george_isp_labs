package ex1;

public class Coffee{
    private int temp;
    private int conc;
    private static int nrOfInstance;
    Coffee(int t,int c){temp = t;conc = c;nrOfInstance++;}

    public static int getNrOfInstance() {
        return nrOfInstance;
    }

    int getTemp(){return temp;}
    int getConc(){return conc;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]";}
}