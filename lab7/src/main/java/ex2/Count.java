package ex2;

import java.io.*;

public class Count {
    public static void main(String[] args) throws FileNotFoundException {
        int count=0;
        try(BufferedReader in =new BufferedReader(new FileReader("D:/George/ss/30123_1_chis_george_isp_labs/lab7/src/main/java/ex2/data.txt"))){
            String s;
            while((s=in.readLine())!=null){
                for(String letter : s.split("")){
                    if(letter.equals(args[0])){
                        count++;
                    }
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("Letter : "+args[0]+" appears "+count+"times");
    }
}
