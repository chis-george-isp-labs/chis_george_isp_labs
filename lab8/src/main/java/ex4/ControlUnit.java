package ex4;

import ex4.NoEvent.EventType;

import java.net.HttpCookie;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public class ControlUnit extends Home{
    private static final Logger LOGGER=Logger.getLogger(ControlUnit.class);
    private final GSMUnit gsmUnit=new GSMUnit();
    private final AlarmUnit alarmUnit=new AlarmUnit();
    private final CoolingUnit coolingUnit=new CoolingUnit();
    private final HeatingUnit heatingUnit=new HeatingUnit();
    private static final int PRESET_TEMPERATURE_VALUE=25;



    @Override
    protected void setValueInEnvironment(Event event) {
    switch(event.getType()){
        case TEMPERATURE:
            this.temperatureSensor.setValue(((TemperatureEvent)event).getValue());
            break;
        case FIRE:
            this.fireSensor.get(RANDOM.nextInt(fireSensor.size())).setValue(((FireEvent)event).isSmoke()?1:0);
            break;
        case NONE:
            break;
        default:
            throw new IllegalAccessException("Unexpected value: "+event.getType());
    }

    }

    @Override
    protected void controllStep() {
        LOGGER.info("Control step:");
        fireSensor.stream()
                .filter(sensor -> sensor.getValue()==1)
                .findAny()
                ifPresent(sensor->{
                    alarmUnit.execute();
                    gsmUnit.execute();
                });
        if(temperatureSensor.getValue()<PRESET_TEMPERATURE_VALUE){
            heatingUnit.execute();
            else{coolingUnit.execute();

        }
            LOGGER.info("");
            fireSensor.forEach(sensor-> sensor.setValue(0));
    }
}
