package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class ProductController {

    private static final Vector<ProductModel> products = new Vector(0);

    public static void main(String[] args) {
        JFrame frame = new JFrame("Product Interface");
        var productInterface = new ProductView();
        productInterface.getADDButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = productInterface.getTextField1().getText();
                int quantity = Integer.parseInt(productInterface.getTextField3().getText());
                double price = Double.parseDouble(productInterface.getTextField3().getText());
                products.add(new ProductModel(name, quantity, price));
            }
        });
        productInterface.getADDButton().addActionListener(e -> productInterface.getList1().setListData(products));
        productInterface.getDeleteSelectedProductButton().addActionListener(e -> products.remove(productInterface.getList1().getSelectedValuesList()));
        productInterface.getUpdateSelectedProductSButtonQuantity().addActionListener(e -> products.get(productInterface.getList1().getSelectedIndex()));
        frame.setContentPane(productInterface.getPanel1());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
