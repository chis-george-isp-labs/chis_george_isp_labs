package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProductView {
    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField3;
    private JTextField textField4;
    private JButton deleteSelectedProductButton;
    private JTextField textField5;
    private JButton updateSelectedProductSButton;
    private JTextField textField6;
    private JButton ADDButton;
    private JList list1;
    private JTextArea textArea1;
    private JButton updateSelctedProductSButton;
    public JButton getDeleteSelectedProductButton() {
        return deleteSelectedProductButton;
    }
    public JTextField getTextField5() {
        return textField5;
    }
    public JButton getUpdateSelctedProductSButton() {
        return updateSelctedProductSButton;
    }
    public JList getList1() {
        return list1;
    }
    public JButton getUpdateSelectedProductSButton() {
        return updateSelectedProductSButton;
    }
    public JTextField getTextField6() {
        return textField6;
    }
    public JButton getADDButton() {
        return ADDButton;
    }
    public JTextArea getTextArea1() {
        return textArea1;
    }
    public JButton getUpdateSelectedProductSButtonQuantity() {
        return updateSelectedProductSButton;
    }
    public JPanel getPanel1() {
        return panel1;
    }
    public JTextField getTextField3() {
        return textField3;
    }
    public JTextField getTextField4() {
        return textField4;
    }
    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }
    public JTextField getTextField1() {
        return textField1;
    }
    public ProductView() {
      ADDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea1.setText(" Product: " + textField1.getText()+", quantity: "+ textField3.getText()+ ", price: " +textField4.getText());
               textArea1.append(" Product: " + textField1.getText()+", quantity: "+ textField3.getText()+ ", price: " +textField4.getText()+ "\n");
            }
       });
        deleteSelectedProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea1.setText(textArea1.getText().replace(textArea1.getSelectedText(), ""));
            }
        });
    }
}
