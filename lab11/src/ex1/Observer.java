package ex1;

interface Observer {
    void update(Object event);
}