package ex1;

import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame implements Observer{
    JTextField jTextField;

    public Interface() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(360,100);
        setLayout(new GridLayout(1,3));
        setVisible(true);
        this.jTextField=new JTextField("aaaaaa");
        this.add(jTextField);
    }

    @Override
    public void update(Object event) {
        jTextField.setText(event.toString());
    }
}
