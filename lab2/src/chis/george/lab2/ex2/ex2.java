package chis.george.lab2.ex2;

import java.util.Scanner;

public class ex2 {
    public static void main(String[] args) {
        System.out.println("Enter a value between 0 and 10:");
        Scanner in = new Scanner(System.in);

        int x = in.nextInt();
        int y;


        switch (x) {
            case 0: {
                y = 0;
                System.out.println("ZERO");
            }
            break;
            case 1: {
                y = 1;
                System.out.println("ONE");
            }
            break;
            case 2: {
                y = 2;
                System.out.println("TWO");
            }
            break;
            case 3: {
                y = 3;
                System.out.println("THREE");
            }
            break;
            case 4: {
                y = 4;
                System.out.println("FOUR");
            }
            break;
            case 5: {
                y = 5;
                System.out.println("FIVE");
            }
            break;
            case 6: {
                y = 6;
                System.out.println("SIX");
            }
            break;
            case 7: {
                y = 7;
                System.out.println("SEVEN");
            }
            break;
            case 8: {
                y = 8;
                System.out.println("EIGHT");
            }
            break;
            case 9: {
                y = 9;
                System.out.println("NINE");
            }
            break;
            case 10: {
                y = 10;
                System.out.println("TEN");
            }
            break;
            default: {
                y = -1;
                System.out.println("OTHER");
            }
            break;

        }
    }
}
