package chis.george.lab2.ex3;

import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        System.out.println("Enter two values:");
        Scanner in = new Scanner(System.in);
        int i, j, ok, nr = 0;
        int x = in.nextInt();
        int y = in.nextInt();
        if (y > x) {
            i = y;
            y = x;        //y will be always the smaller or the equal
            x = i;
        }
        for (i = y; i <= x; i++) {
            ok = 0;

            for (j = 2; j <= i / 2; j++)
                if (i % j == 0) ok = 1;

            if (ok == 0) {
                System.out.println(i + " ");
                nr++;
            }
        }
        System.out.println(nr + " prime numbers were found.");

    }
}
