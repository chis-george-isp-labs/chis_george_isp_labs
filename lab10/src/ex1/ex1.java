package ex1;

public class ex1 extends Thread {

    ex1(String name){
        super(name);
    }

    public void run(){
        for(int i=0;i<20;i++){
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }

    public static void main(String[] args) {
        ex1 c1 = new ex1("counter1");
        ex1 c2 = new ex1("counter2");
        ex1 c3 = new ex1("counter3");

        c1.start();
        c2.start();
        c3.start();
    }
}