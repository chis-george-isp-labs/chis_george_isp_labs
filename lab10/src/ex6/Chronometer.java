package ex6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

public class Chronometer extends JFrame  {
    JButton jButton1,jButton2;
    JTextField jTextField1,jTextField2,jTextField3;
    public int m=0,s=0,ms=0,nr=1;
    Chronometer(){
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        setVisible(true);
        init();
    }

    public void init(){
        this.setLayout(null);
        jButton1=new JButton("Reset");
        jButton1.setBounds(10,150,100,40);
        jButton2=new JButton("Start/Stop");
        jButton2.setBounds(200,150,100,40);
        jTextField1=new JTextField("");
        jTextField1.setBounds(30,30,40,40);
        jTextField2=new JTextField("");
        jTextField2.setBounds(80,30,40,40);
        jTextField3=new JTextField("");
        jTextField3.setBounds(130,30,40,40);
        add(jButton1);
        add(jTextField1);
        add(jTextField2);
        add(jTextField3);
        add(jButton2);
        Scanner scanner=new Scanner(System.in);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        jButton1.addActionListener(e -> {           //Reset
            if(e.getSource() == jButton1 ){
                m=0;s=0;ms=0;nr=1;
                jTextField1.setText(String.valueOf(m));
                jTextField2.setText(String.valueOf(s));
                jTextField3.setText(String.valueOf(ms));
            }
        });
        jButton2.addActionListener(e -> {           //Start/Stop
            if(e.getSource() == jButton2 && nr%2==1)        //Start
            {synchronized (this){
                while(ms<101)
                {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    if (ms == 100) ms = 0;
                    jTextField1.setText(String.valueOf(ms));
                    ms++;
                    if(e.getSource()==jButton2 && nr%2==0)break;
                }
                while(s<61)
                {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    if(ms==60)s=0;
                    jTextField2.setText(String.valueOf(s));
                    s++;
                    if(e.getSource()==jButton2 && nr%2==0)break;

                }
                while(m<61)
                {
                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    if(m==60)m=0;
                    jTextField3.setText(String.valueOf(m));
                    m++;
                    if(e.getSource()==jButton2 && nr%2==0)break;
                }
            }

            nr++;
            }
            else if(e.getSource()==jButton2 && nr%2==0)         //Stop
            {synchronized (this){
                try {
                    wait();
                    scanner.equals(e.getSource()==jButton2 && nr%2==1);
                    notify();
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
            nr++;
            }
        });

    }

    public static void main(String[] args) {
        new Chronometer();
    }


}
