package ex4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        SetPosition setPosition=new SetPosition(null);
        List<Robot> robotList=new ArrayList<>(10);
        initializeRobots(robotList);
        VerifyThread verifyThread=new VerifyThread(robotList);
        setPosition.start();
        verifyThread.start();
    }
    private static void initializeRobots(List<Robot> robotList){
        for (int i = 0; i < 10; i++) {
            robotList.add(new Robot( i ,i*2));
        }
    }
}
