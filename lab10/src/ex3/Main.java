package ex3;

public class Main {
    public static void main(String[] args) {
        Counter1 counter1 = new Counter1(null);
        Counter2 counter2 = new Counter2(counter1);
        counter1.start();
        counter2.start();
    }
}
