package ex3;

public class Counter2 extends Thread {
    Thread thread2;
    public int n = 100;

    Counter2(Thread thread2) {
        this.thread2 = thread2;
    }

    public void run() {
        while (n <= 200) {
            try {
                thread2.join();
                System.out.println(getName() + " " + n);
                n++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
