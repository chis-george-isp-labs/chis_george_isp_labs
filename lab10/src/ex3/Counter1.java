package ex3;

public class Counter1 extends Thread {
    Thread thread1;
    public int n;

    Counter1(Thread thread1) {
        this.thread1 = thread1;
    }

    public void run() {
        while (n <= 100) {
            System.out.println(getName() + " " + n);
            n++;
        }
    }
}
