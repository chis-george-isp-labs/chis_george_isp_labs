package ex4;
import ex4.Dictionary;
public class Main {


    public static void main(String[] args) {
        Dictionary dictionary=new Dictionary();
        dictionary.getAllDefinitions();
        dictionary.addWord("image","a representation of the external form of a person or thing in art.");
        dictionary.addWord("picture","a painting or drawing.");
        dictionary.getAllDefinitions();
        dictionary.getAllWords();
        dictionary.getDefinition("picture");

    }
}
