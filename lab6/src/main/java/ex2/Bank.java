package ex2;

import ex1.BankAccount;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class Bank {
    private final List<BankAccount> bankAccounts;

    public Bank() {
        this.bankAccounts = new ArrayList<>();
    }

    public void addAccounts(String owner, double balance) {
        this.bankAccounts.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        for (Object o : bankAccounts) {
            System.out.println("Account " + o);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (int i = 0; i < this.bankAccounts.size(); i++) {
            if (this.bankAccounts.get(i).getBalance() > minBalance && this.bankAccounts.get(i).getBalance() < maxBalance)
                System.out.println("Account number "+ i+ " "+this.bankAccounts.get(i).toString());
        }


    }

    public BankAccount getAccount(String owner) {
        for (int i = 0; i < bankAccounts.size(); i++) {
            if (this.bankAccounts.get(i).getOwner() == owner) {
                System.out.println("Account number " + i);
                break;
            }
        }

        return null;
    }

    public void getAllAccounts(){
        for (int i = 0; i < this.bankAccounts.size()-1; i++)
            for (int j = i; j < this.bankAccounts.size()-1; j++) {
            if(this.bankAccounts.get(j).getOwner().compareTo(this.bankAccounts.get(j+1).getOwner())>0)
                Collections.swap(bankAccounts,j,j+1);

        }



    }

}
