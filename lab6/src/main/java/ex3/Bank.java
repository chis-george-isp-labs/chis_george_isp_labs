package ex3;

import ex3.BankAccount;

import java.util.*;


public class Bank {
    private final Set<ex3.BankAccount> bankAccounts;

    public Bank() {
        this.bankAccounts = new TreeSet<>();
    }

    public void addAccounts(String owner, double balance) {
        this.bankAccounts.add(new ex3.BankAccount(owner, balance));
    }

    public void printAccounts() {
        for (Object o : bankAccounts) {
            System.out.println("Account " + o);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
       this.bankAccounts.stream()
               .filter(bankAccount -> bankAccount.getBalance()>= minBalance && bankAccount.getBalance()<=maxBalance)
               .forEach(System.out::println);

    }

    public BankAccount getAccount(String owner) {
      return  bankAccounts.stream()
                .filter(bankAccount -> bankAccount.getOwner().equals(owner))
                .findFirst()
                .orElse(null);
    }




}
