package ex3;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;
    public BankAccount(){

    }
    public BankAccount(String owner,double balance){
        this.balance=balance;
        this.owner=owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void withdraw(double amount){
        if(amount<=this.balance)this.balance=this.balance-amount;
        else System.out.println("Error! Not enough credit!");
    }

    public void deposit(double amount){
        this.balance=this.balance+amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ex3.BankAccount that = (ex3.BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{"+
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    public static void main(String[] args) {
        ex1.BankAccount bankAccount=new ex1.BankAccount("john",50);
        bankAccount.withdraw(20);
        System.out.println(bankAccount.getBalance());
        bankAccount.deposit(100);
        System.out.println(bankAccount.getBalance());
    }
}
