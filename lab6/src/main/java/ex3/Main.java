package ex3;

import ex3.Bank;

public class Main {


    public static void main(String[] args) {
        Bank bank=new Bank();
        bank.addAccounts("Alex",5000);
        bank.addAccounts("John",2000);
        bank.addAccounts("Doe",1500);
        bank.addAccounts("Zac",6500);
        bank.addAccounts("Erdith",1000);
        bank.addAccounts("Lucas",3000);
        bank.addAccounts("Zoe",500);
        bank.addAccounts("Max",3000);
        bank.printAccounts();
        bank.printAccounts(1000,6000);
        bank.getAccount("Alex");
        bank.getAccount("Max");


    }
}
