package ex2;

import javax.swing.*;

public class Increment extends JFrame {
    public int text=0;
    JButton button;
    JTextField textField;
    Increment(){
        setTitle("Test login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
    }
    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;


        textField = new JTextField();
        textField.setBounds(70,50,width, height);


        button = new JButton("Increment the number");
        button.setBounds(10,150,width, height);

        add(textField);add(button);
        button.addActionListener(e->{
            if(e.getSource()==button){text++;
            textField.setText(String.valueOf(text));}
        });
        textField.setText(String.valueOf(text));
    }

    public static void main(String[] args) {
        new Increment();
    }
}
