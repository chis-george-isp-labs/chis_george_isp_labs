//package ex3;
//
//import javax.swing.*;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.nio.file.Files;
//import java.nio.file.Path;
//
//public class Display {
//    private JTextField textField;
//    private JButton jButton;
//    private JTextArea textArea;
//    private JPanel panel;
//    public Display(){
//        textField.setText("src/ex3/Displayfile.txt");
//        jButton.addActionListener(e ->  {
//
//                try {
//                    textArea.setText(Files.readString(Path.of(textField.getText()), StandardCharsets.US_ASCII));
//                } catch (IOException ioException) {
//                    System.out.println("File not found");
//                }
//
//        });
//    }
//
//    public static void main(String[] args) {
//        JFrame frame=new JFrame("Display");
//        frame.setContentPane(new Display().panel);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.pack();
//        frame.setVisible(true);
//    }
//}
